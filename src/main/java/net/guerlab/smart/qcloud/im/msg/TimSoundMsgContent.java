package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 语音消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimSoundMsgContent extends AbstractMsgContent {

    /**
     * 语音下载地址，可通过该 URL 地址直接下载相应语音。
     */
    @JsonProperty("Url")
    private String url;

    /**
     * 语音数据大小，单位：字节。
     */
    @JsonProperty("Size")
    private String size;

    /**
     * 语音时长，单位：秒。
     */
    @JsonProperty("Second")
    private String second;

    /**
     * 语音下载方式标记。目前 Download_Flag 取值只能为2，表示可通过Url字段值的 URL 地址直接下载语音。
     */
    @JsonProperty("Download_Flag")
    private String downloadFlag;
}
