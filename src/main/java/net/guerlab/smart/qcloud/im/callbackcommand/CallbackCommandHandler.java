package net.guerlab.smart.qcloud.im.callbackcommand;

/**
 * 回调结果处理
 *
 * @author guer
 */
public interface CallbackCommandHandler {

    /**
     * 是否处理
     *
     * @param callbackCommand
     *         回调命令
     * @return 是否处理
     */
    boolean accept(AbstractCallbackCommand callbackCommand);

    /**
     * 处理回调
     *
     * @param callbackCommand
     *         回调命令
     * @return 回调结果
     */
    CallbackResult handler(AbstractCallbackCommand callbackCommand);
}
