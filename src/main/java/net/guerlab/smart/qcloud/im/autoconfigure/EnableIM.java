package net.guerlab.smart.qcloud.im.autoconfigure;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用IM
 *
 * @author guer
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({ ImAutoconfigure.class })
public @interface EnableIM {}
