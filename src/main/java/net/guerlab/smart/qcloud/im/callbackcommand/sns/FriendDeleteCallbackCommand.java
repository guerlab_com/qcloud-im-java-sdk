package net.guerlab.smart.qcloud.im.callbackcommand.sns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

import java.util.List;

/**
 * 删除好友之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FriendDeleteCallbackCommand extends AbstractCallbackCommand {

    /**
     * 成功删除的好友
     */
    @JsonProperty("PairList")
    private List<Pair> pairList;
}
