package net.guerlab.smart.qcloud.im.msg;

/**
 * 文本消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class TimTextMsg extends AbstractMsgBody<TimTextMsgContent> {

    public TimTextMsg() {
        super(MsgType.TIMTextElem);
    }
}
