package net.guerlab.smart.qcloud.im.callbackcommand;

import lombok.Getter;

/**
 * 请求处理的结果
 *
 * @author guer
 */
@Getter
public enum ActionStatus {
    /**
     * 成功
     */
    OK(0),
    /**
     * 失败
     */
    FAIL(1);

    private final int errorCode;

    ActionStatus(int errorCode) {
        this.errorCode = errorCode;
    }
}
