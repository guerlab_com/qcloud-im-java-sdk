package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

import java.util.List;

/**
 * 群组资料修改之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AfterGroupInfoChangedCallbackCommand extends AbstractCallbackCommand {

    /**
     * 操作的群 ID
     */
    @JsonProperty("GroupId")
    private String groupId;

    /**
     * 产生群消息的 群组形态介绍，例如 Private，Public 和 ChatRoom
     */
    @JsonProperty("Type")
    private String type;

    /**
     * 发起创建群组请求的操作者 Identifier
     */
    @JsonProperty("Operator_Account")
    private String operatorAccount;

    /**
     * 修改后的群名称
     */
    @JsonProperty("Name")
    private String name;

    /**
     * 修改后的群简介
     */
    @JsonProperty("Introduction")
    private String introduction;

    /**
     * 修改后的群公告
     */
    @JsonProperty("Notification")
    private String notification;

    /**
     * 修改后的群头像 URL
     */
    @JsonProperty("FaceUrl")
    private String faceUrl;

    /**
     * 被修改的群维度自定义字段
     */
    @JsonProperty("UserDefinedDataList")
    private List<GroupUserDefinedData> userDefinedDataList;
}
