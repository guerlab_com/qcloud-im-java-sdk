package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 表情消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimFaceMsgContent extends AbstractMsgContent {

    /**
     * 表情索引，用户自定义。
     */
    @JsonProperty("Index")
    private Integer index;

    /**
     * 额外数据。
     */
    @JsonProperty("Data")
    private String data;
}
