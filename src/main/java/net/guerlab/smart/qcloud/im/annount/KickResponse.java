package net.guerlab.smart.qcloud.im.annount;

import net.guerlab.smart.qcloud.im.AbstractImResponse;

/**
 * 帐号登录态失效响应
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class KickResponse extends AbstractImResponse {}
