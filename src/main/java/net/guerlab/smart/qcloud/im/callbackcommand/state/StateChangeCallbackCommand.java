package net.guerlab.smart.qcloud.im.callbackcommand.state;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

/**
 * 状态变更回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StateChangeCallbackCommand extends AbstractCallbackCommand {

    /**
     * 用户上下线的信息
     */
    @JsonProperty("Info")
    private StateChangeInfo info;
}
