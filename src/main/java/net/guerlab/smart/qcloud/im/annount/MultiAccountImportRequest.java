package net.guerlab.smart.qcloud.im.annount;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.AbstractImRequest;
import net.guerlab.smart.qcloud.im.ImException;

import java.util.HashMap;
import java.util.List;

/**
 * 批量导入用户请求
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MultiAccountImportRequest extends AbstractImRequest<MultiAccountImportResponse> {

    /**
     * 用户名，单个用户名长度不超过32字节，单次最多导入100个用户名
     */
    private List<String> accounts;

    @Override
    public String uri() {
        return "v4/im_open_login_svc/multiaccount_import";
    }

    @Override
    public byte[] requestBody(ObjectMapper objectMapper) {
        try {
            HashMap<String, Object> jsonObject = new HashMap<>(1);

            jsonObject.put("Accounts", accounts);

            return objectMapper.writeValueAsBytes(jsonObject);
        } catch (Exception e) {
            throw new ImException(e.getLocalizedMessage(), e);
        }
    }
}
