package net.guerlab.smart.qcloud.im.callbackcommand;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 应答包
 *
 * @author guer
 */
@Data
public class CallbackResult {

    /**
     * 请求处理的结果
     */
    @JsonProperty("ActionStatus")
    private ActionStatus actionStatus = ActionStatus.OK;

    /**
     * 错误码，0表示 App 后台处理成功，1表示 App 后台处理失败
     */
    @JsonProperty("ErrorCode")
    private Integer errorCode = ActionStatus.OK.getErrorCode();

    /**
     * 错误信息
     */
    @JsonProperty("ErrorInfo")
    private String errorInfo = "";
}
