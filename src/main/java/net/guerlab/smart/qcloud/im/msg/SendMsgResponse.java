package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.AbstractImResponse;

/**
 * 发送消息响应
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SendMsgResponse extends AbstractImResponse {

    /**
     * 消息时间戳，UNIX 时间戳
     */
    @JsonProperty("MsgTime")
    private Integer msgTime;

    /**
     * 消息Key
     */
    @JsonProperty("MsgKey")
    private String msgKey;
}
