package net.guerlab.smart.qcloud.im.callbackcommand.sns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 好友关系
 *
 * @author guer
 */
@Data
public class Pair {

    /**
     * 源用户
     */
    @JsonProperty("From_Account")
    private String fromAccount;

    /**
     * 目标用户
     */
    @JsonProperty("To_Account")
    private String toAccount;

    /**
     * 发起用户
     */
    @JsonProperty("Initiator_Account")
    private String initiatorAccount;
}
