package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文本消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimTextMsgContent extends AbstractMsgContent {

    /**
     * 消息内容。当接收方为 iOS 或 Android 后台在线时，作为离线推送的文本展示。
     */
    @JsonProperty("Text")
    private String text;
}
