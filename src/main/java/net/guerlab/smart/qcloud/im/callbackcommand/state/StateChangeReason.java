package net.guerlab.smart.qcloud.im.callbackcommand.state;

/**
 * 用户上下线触发的原因
 *
 * @author guer
 */
public enum StateChangeReason {

    /**
     * App TCP 连接建立
     */
    Register,
    /**
     * App 用户注销帐号导致 TCP 断开
     */
    Unregister,
    /**
     * 即时通信 IM 检测到 App TCP 连接断开
     */
    LinkClose,
    /**
     * 即时通信 IM 检测到 App 心跳包超时，认为 TCP 已断开（客户端杀后台或 Crash）
     */
    TimeOut
}
