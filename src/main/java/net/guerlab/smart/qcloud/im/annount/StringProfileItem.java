package net.guerlab.smart.qcloud.im.annount;

/**
 * 资料设置项-string类型
 *
 * @author guer
 */
public class StringProfileItem extends ProfileItem<String> {

    /**
     * 无参数构造
     */
    public StringProfileItem() {
    }

    /**
     * 通过指定标签和值来构造一个资料设置项
     *
     * @param tag
     *         标签
     * @param value
     *         值
     */
    public StringProfileItem(String tag, String value) {
        super(tag, value);
    }
}
