package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 地理位置消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimLocationMsgContent extends AbstractMsgContent {

    /**
     * 地理位置描述信息。
     */
    @JsonProperty("Desc")
    private String desc;

    /**
     * 纬度。
     */
    @JsonProperty("Latitude")
    private double latitude;

    /**
     * 经度。
     */
    @JsonProperty("Longitude")
    private double longitude;
}
