package net.guerlab.smart.qcloud.im.msg;

/**
 * 自定义消息
 *
 * @author guer
 */
public class TimCustomMsg extends AbstractMsgBody<TimCustomMsgContent> {

    public TimCustomMsg() {
        super(MsgType.TIMCustomElem);
    }
}
