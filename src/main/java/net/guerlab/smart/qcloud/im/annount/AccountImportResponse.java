package net.guerlab.smart.qcloud.im.annount;

import net.guerlab.smart.qcloud.im.AbstractImResponse;

/**
 * 导入用户响应
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class AccountImportResponse extends AbstractImResponse {}
