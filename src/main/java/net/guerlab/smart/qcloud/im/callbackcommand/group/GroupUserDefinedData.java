package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 用户建群时的自定义字段
 *
 * @author guer
 */
@Data
public class GroupUserDefinedData {

    /**
     * key
     */
    @JsonProperty("Key")
    private String key;

    /**
     * value
     */
    @JsonProperty("Value")
    private String value;
}
