package net.guerlab.smart.qcloud.im;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 抽象响应数据
 *
 * @author guer
 */
@Setter
@Getter
@ToString
public abstract class AbstractImResponse {

    /**
     * 请求的处理结果
     */
    @JsonProperty("ActionStatus")
    private ActionStatus actionStatus;

    /**
     * 错误码
     */
    @JsonProperty("ErrorCode")
    private Integer errorCode;

    /**
     * 请求处理失败时的错误信息
     */
    @JsonProperty("ErrorInfo")
    private String errorInfo;
}
