package net.guerlab.smart.qcloud.im.msg;

/**
 * 表情消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class TimFaceMsg extends AbstractMsgBody<TimFaceMsgContent> {

    public TimFaceMsg() {
        super(MsgType.TIMFaceElem);
    }
}
