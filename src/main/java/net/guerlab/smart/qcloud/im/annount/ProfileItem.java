package net.guerlab.smart.qcloud.im.annount;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 资料设置项
 *
 * @author guer
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfileItem<V> {

    /**
     * 描述：昵称
     * 类型：string
     * 备注：长度不得超过500个字节
     */
    public static final String TAG_PROFILE_IM_NICK = "Tag_Profile_IM_Nick";

    /**
     * 描述：性别
     * 类型：string
     * 备注：
     * Gender_Type_Unknown：没设置性别
     * Gender_Type_Female：女性
     * Gender_Type_Male：男性
     */
    public static final String TAG_PROFILE_IM_GENDER = "Tag_Profile_IM_Gender";

    /**
     * 描述：uint32
     * 类型：生日
     * 备注：推荐用法：20190419
     */
    public static final String TAG_PROFILE_IM_BIRTHDAY = "Tag_Profile_IM_BirthDay";

    /**
     * 描述：string
     * 类型：所在地
     * 备注：长度不得超过16个字节，推荐用法如下：
     * App 本地定义一套数字到地名的映射关系
     * 后台实际保存的是4个 uint32_t 类型的数字
     * 其中第一个 uint32_t 表示国家
     * 第二个 uint32_t 用于表示省份
     * 第三个 uint32_t 用于表示城市
     * 第四个 uint32_t 用于表示区县
     */
    public static final String TAG_PROFILE_IM_LOCATION = "Tag_Profile_IM_Location";

    /**
     * 描述：个性签名
     * 类型：string
     * 备注：长度不得超过500个字节
     */
    public static final String TAG_PROFILE_IM_SELF_SIGNATURE = "Tag_Profile_IM_SelfSignature";

    /**
     * 描述：string
     * 类型：加好友验证方式
     * 备注：
     * AllowType_Type_NeedConfirm：需要经过自己确认才能添加自己为好友
     * AllowType_Type_AllowAny：允许任何人添加自己为好友
     * AllowType_Type_DenyAny：不允许任何人添加自己为好友
     */
    public static final String TAG_PROFILE_IM_ALLOW_TYPE = "Tag_Profile_IM_AllowType";

    /**
     * 描述：语言
     * 类型：uint32
     * 备注：
     */
    public static final String TAG_PROFILE_IM_LANGUAGE = "Tag_Profile_IM_Language";

    /**
     * 描述：头像URL
     * 类型：string
     * 备注：长度不得超过500个字节
     */
    public static final String TAG_PROFILE_IM_IMAGE = "Tag_Profile_IM_Image";

    /**
     * 描述：消息设置
     * 类型：uint32
     * 备注：标志位：
     * Bit0：置0表示接收消息，置1则不接收消息
     */
    public static final String TAG_PROFILE_IM_MSG_SETTINGS = "Tag_Profile_IM_MsgSettings";

    /**
     * 描述：管理员禁止加好友标识
     * 类型：string
     * 备注：
     * AdminForbid_Type_None：默认值，允许加好友
     * AdminForbid_Type_SendOut：禁止该用户发起加好友请求
     */
    public static final String TAG_PROFILE_IM_ADMIN_FORBID_TYPE = "Tag_Profile_IM_AdminForbidType";

    /**
     * 描述：等级
     * 类型：uint32
     * 备注：建议拆分以保存多种角色的等级信息
     */
    public static final String TAG_PROFILE_IM_LEVEL = "Tag_Profile_IM_Level";

    /**
     * 描述：角色
     * 类型：uint32
     * 备注：建议拆分以保存多种角色信息
     */
    public static final String TAG_PROFILE_IM_ROLE = "Tag_Profile_IM_Role";

    /**
     * 指定要设置的资料字段的名称
     */
    @JsonProperty("Tag")
    private String tag;

    /**
     * 待设置的资料字段的值
     */
    @JsonProperty("Value")
    private V value;
}
