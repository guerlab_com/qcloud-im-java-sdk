package net.guerlab.smart.qcloud.im.autoconfigure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * Im配置
 *
 * @author guer
 */
@Data
@RefreshScope
@ConfigurationProperties(prefix = "im")
public class ImProperties {

    /**
     * sdkAppId
     */
    private Long sdkAppId;

    /**
     * key
     */
    private String key;

    /**
     * identifier
     */
    private String identifier;
}
