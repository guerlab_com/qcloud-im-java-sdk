package net.guerlab.smart.qcloud.im.callbackcommand.c2c;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;
import net.guerlab.smart.qcloud.im.msg.AbstractMsgBody;

import java.util.List;

/**
 * 发单聊消息之前回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ClientToClientBeforeSendMsgCallbackCommand extends AbstractCallbackCommand {

    /**
     * 消息发送者 Identifier
     */
    @JsonProperty("From_Account")
    private String fromAccount;

    /**
     * 消息接收者 Identifier
     */
    @JsonProperty("To_Account")
    private String toAccount;

    /**
     * 消息序列号，用于标记该条消息
     */
    @JsonProperty("MsgSeq")
    private Integer msgSeq;

    /**
     * 消息随机数，用于标记该条消息
     */
    @JsonProperty("MsgRandom")
    private Integer msgRandom;

    /**
     * 消息的发送时间戳，单位为秒
     */
    @JsonProperty("MsgTime")
    private Integer msgTime;

    /**
     * 消息体
     */
    @JsonProperty("MsgBody")
    private List<AbstractMsgBody<?>> msgBody;
}
