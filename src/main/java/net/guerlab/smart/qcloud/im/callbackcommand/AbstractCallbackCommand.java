package net.guerlab.smart.qcloud.im.callbackcommand;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 抽象回调命令
 *
 * @author guer
 */
@Data
public class AbstractCallbackCommand {

    /**
     * 回调命令
     */
    @JsonProperty("CallbackCommand")
    private String callbackCommand;

}
