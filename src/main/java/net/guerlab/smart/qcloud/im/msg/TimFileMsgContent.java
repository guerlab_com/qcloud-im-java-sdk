package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文件消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimFileMsgContent extends AbstractMsgContent {

    /**
     * 文件下载地址，可通过该 URL 地址直接下载相应文件。
     */
    @JsonProperty("Url")
    private String url;

    /**
     * 文件数据大小，单位：字节。
     */
    @JsonProperty("FileSize")
    private String fileSize;

    /**
     * 文件名称。
     */
    @JsonProperty("FileName")
    private String fileName;

    /**
     * 文件下载方式标记。目前 Download_Flag 取值只能为2，表示可通过Url字段值的 URL 地址直接下载文件。
     */
    @JsonProperty("Download_Flag")
    private String downloadFlag;
}
