package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 图像消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimImageMsgContent extends AbstractMsgContent {

    /**
     * 图片序列号。后台用于索引图片的键值。
     */
    @JsonProperty("UUID")
    private String uuid;

    /**
     * 图片格式。JPG = 1，GIF = 2，PNG = 3，BMP = 4，其他 = 255。
     */
    @JsonProperty("ImageFormat")
    private Long imageFormat;

    /**
     * 原图、缩略图或者大图下载信息。
     */
    @JsonProperty("ImageInfoArray")
    private List<ImageInfo> imageInfoArray = new ArrayList<>();

    /**
     * 图像下载信息
     */
    @SuppressWarnings("WeakerAccess")
    @Data
    public static class ImageInfo {

        /**
         * 图片类型： 1-原图，2-大图，3-缩略图。
         */
        @JsonProperty("Type")
        private Long type;

        /**
         * 图片数据大小，单位：字节。
         */
        @JsonProperty("Size")
        private Long size;

        /**
         * 图片宽度。
         */
        @JsonProperty("Width")
        private Long width;

        /**
         * 图片高度。
         */
        @JsonProperty("Height")
        private Long height;

        /**
         * 图片下载地址。
         */
        @JsonProperty("URL")
        private Long url;
    }
}
