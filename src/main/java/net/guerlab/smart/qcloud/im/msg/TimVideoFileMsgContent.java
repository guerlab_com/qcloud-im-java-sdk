package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimVideoFileMsgContent extends AbstractMsgContent {

    /**
     * 视频下载地址。可通过该 URL 地址直接下载相应视频。
     */
    @JsonProperty("VideoUrl")
    private String videoUrl;

    /**
     * 视频数据大小，单位：字节。
     */
    @JsonProperty("VideoSize")
    private Long videoSize;

    /**
     * 视频时长，单位：秒。
     */
    @JsonProperty("VideoSecond")
    private Long videoSecond;

    /**
     * 视频格式，例如 mp4。
     */
    @JsonProperty("VideoFormat")
    private String videoFormat;

    /**
     * 视频下载方式标记。目前 VideoDownloadFlag 取值只能为2，表示可通过VideoUrl字段值的 URL 地址直接下载视频。
     */
    @JsonProperty("VideoDownloadFlag")
    private Long videoDownloadFlag;

    /**
     * 视频缩略图下载地址。可通过该 URL 地址直接下载相应视频缩略图。
     */
    @JsonProperty("ThumbUrl")
    private String thumbUrl;

    /**
     * 缩略图大小，单位：字节。
     */
    @JsonProperty("ThumbSize")
    private Long thumbSize;

    /**
     * 缩略图宽度。
     */
    @JsonProperty("ThumbWidth")
    private Long thumbWidth;

    /**
     * 缩略图高度。
     */
    @JsonProperty("ThumbHeight")
    private Long thumbHeight;

    /**
     * 缩略图格式，例如 JPG、BMP 等。
     */
    @JsonProperty("ThumbFormat")
    private String thumbFormat;

    /**
     * 视频缩略图下载方式标记。目前 ThumbDownloadFlag 取值只能为2，表示可通过ThumbUrl字段值的 URL 地址直接下载视频缩略图。
     */
    @JsonProperty("ThumbDownloadFlag")
    private String thumbDownloadFlag;
}
