package net.guerlab.smart.qcloud.im.msg;

/**
 * 语音消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class TimSoundMsg extends AbstractMsgBody<TimSoundMsgContent> {

    public TimSoundMsg() {
        super(MsgType.TIMSoundElem);
    }
}
