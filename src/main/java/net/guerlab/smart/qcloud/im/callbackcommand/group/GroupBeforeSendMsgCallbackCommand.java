package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;
import net.guerlab.smart.qcloud.im.msg.AbstractMsgBody;

import java.util.List;

/**
 * 群内发言之前回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GroupBeforeSendMsgCallbackCommand extends AbstractCallbackCommand {

    /**
     * 产生群消息的群组 ID
     */
    @JsonProperty("GroupId")
    private String groupId;

    /**
     * 产生群消息的 群组形态介绍，例如 Private，Public 和 ChatRoom
     */
    @JsonProperty("Type")
    private String type;

    /**
     * 消息发送者 Identifier
     */
    @JsonProperty("From_Account")
    private String fromAccount;

    /**
     * 请求的发起者
     */
    @JsonProperty("Operator_Account")
    private String operatorAccount;

    /**
     * 随机数
     */
    @JsonProperty("Random")
    private Integer random;

    /**
     * 消息体
     */
    @JsonProperty("MsgBody")
    private List<AbstractMsgBody<?>> msgBody;
}
