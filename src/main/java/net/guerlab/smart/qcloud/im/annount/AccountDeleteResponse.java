package net.guerlab.smart.qcloud.im.annount;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.AbstractImResponse;

import java.util.List;

/**
 * 删除用户响应
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AccountDeleteResponse extends AbstractImResponse {

    /**
     * 单个帐号的结果对象数组
     */
    @JsonProperty("ResultItem")
    private List<ResultItem> resultItem;

    /**
     * 单个帐号的结果
     */
    @Data
    public static class ResultItem {

        /**
         * 单个帐号的错误码，0表示成功，非0表示失败
         */
        @JsonProperty("ResultCode")
        private Integer resultCode;

        /**
         * 单个帐号删除失败时的错误描述信息
         */
        @JsonProperty("ResultInfo")
        private String resultInfo;

        /**
         * 请求删除的帐号的 UserID
         */
        @JsonProperty("UserID")
        private String userId;
    }
}
