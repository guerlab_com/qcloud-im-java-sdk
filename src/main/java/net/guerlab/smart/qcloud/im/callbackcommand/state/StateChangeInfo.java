package net.guerlab.smart.qcloud.im.callbackcommand.state;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 用户上下线的信息
 *
 * @author guer
 */
@Data
public class StateChangeInfo {

    /**
     * 用户上线或者下线的动作
     */
    @JsonProperty("Action")
    private StateChangeAction action;

    /**
     * 用户 Identifier
     */
    @JsonProperty("To_Account")
    private String toAccount;

    /**
     * 用户上下线触发的原因
     */
    @JsonProperty("Reason")
    private StateChangeReason reason;
}
