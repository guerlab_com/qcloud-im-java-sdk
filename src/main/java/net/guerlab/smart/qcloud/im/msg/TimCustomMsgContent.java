package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义消息
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TimCustomMsgContent extends AbstractMsgContent {

    /**
     * 自定义消息数据。 不作为 APNs 的 payload 字段下发，故从 payload 中无法获取 Data 字段。
     */
    @JsonProperty("Data")
    private String data;

    /**
     * 自定义消息描述信息；当接收方为 iOS 或 Android 后台在线时，做离线推送文本展示。
     */
    @JsonProperty("Desc")
    private String desc;

    /**
     * 扩展字段；当接收方为 iOS 系统且应用处在后台时，此字段作为 APNs 请求包 Payloads 中的 Ext 键值下发，Ext 的协议格式由业务方确定，APNs 只做透传。
     */
    @JsonProperty("Ext")
    private String ext;

    /**
     * 自定义 APNs 推送铃音。
     */
    @JsonProperty("Sound")
    private String sound;
}
