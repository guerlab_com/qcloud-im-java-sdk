package net.guerlab.smart.qcloud.im.callbackcommand.sns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

import java.util.List;

/**
 * 删除黑名单之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BlackListDeleteCallbackCommand extends AbstractCallbackCommand {

    /**
     * 成功添加的黑名单关系链对
     */
    @JsonProperty("PairList")
    private List<Pair> pairList;
}
