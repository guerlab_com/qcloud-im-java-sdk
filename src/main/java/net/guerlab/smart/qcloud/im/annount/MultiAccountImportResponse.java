package net.guerlab.smart.qcloud.im.annount;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.AbstractImResponse;

import java.util.List;

/**
 * 批量导入用户响应
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MultiAccountImportResponse extends AbstractImResponse {

    /**
     * 导入失败的帐号列表
     */
    @JsonProperty("FailAccounts")
    private List<String> failAccounts;
}
