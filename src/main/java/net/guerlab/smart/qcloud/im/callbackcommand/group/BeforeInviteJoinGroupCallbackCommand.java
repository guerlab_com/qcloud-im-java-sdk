package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;
import net.guerlab.smart.qcloud.im.callbackcommand.CallbackResult;

import java.util.List;

/**
 * 申请入群之前回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BeforeInviteJoinGroupCallbackCommand extends AbstractCallbackCommand {

    /**
     * 要将其他用户拉入的群组 ID
     */
    @JsonProperty("GroupId")
    private String groupId;

    /**
     * 产生群消息的 群组形态介绍，例如 Private，Public 和 ChatRoom
     */
    @JsonProperty("Type")
    private String type;

    /**
     * 请求的操作者 Identifier
     */
    @JsonProperty("Operator_Account")
    private String operatorAccount;

    /**
     * 要拉入群组的 Identifier 集合
     */
    @JsonProperty("DestinationMembers")
    private List<GroupMember> destinationMembers;

    /**
     * 申请入群之前回调响应包
     */
    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class BeforeInviteJoinGroupCallbackCommandResult extends CallbackResult {

        /**
         * 拒绝加入的用户 ID 集合
         */
        @JsonProperty("RefusedMembers_Account")
        private List<String> refusedMembersAccount;
    }
}
