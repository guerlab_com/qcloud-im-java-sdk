package net.guerlab.smart.qcloud.im.msg;

/**
 * 图像消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class TimImageMsg extends AbstractMsgBody<TimImageMsgContent> {

    public TimImageMsg() {
        super(MsgType.TIMImageElem);
    }
}
