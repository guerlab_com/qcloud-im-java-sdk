package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

import java.util.List;

/**
 * 群成员离开之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AfterMemberExitCallbackCommand extends AbstractCallbackCommand {

    /**
     * 产生群消息的群组 ID
     */
    @JsonProperty("GroupId")
    private String groupId;

    /**
     * 产生群消息的 群组形态介绍，例如 Private，Public 和 ChatRoom
     */
    @JsonProperty("Type")
    private String type;

    /**
     * 成员离开方式：Kicked-被踢；Quit-主动退群
     */
    @JsonProperty("ExitType")
    private String exitType;

    /**
     * 发起创建群组请求的操作者 Identifier
     */
    @JsonProperty("Operator_Account")
    private String operatorAccount;

    /**
     * 新入群成员 Identifier 集合
     */
    @JsonProperty("ExitMemberList")
    private List<GroupMember> exitMemberList;
}
