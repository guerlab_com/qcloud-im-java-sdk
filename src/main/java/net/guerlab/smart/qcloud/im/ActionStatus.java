package net.guerlab.smart.qcloud.im;

/**
 * 请求的处理结果
 *
 * @author guer
 */
public enum ActionStatus {
    /**
     * 成功
     */
    OK,

    /**
     * 失败
     */
    FAIL
}
