package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

import java.util.List;

/**
 * 创建群组之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AfterCreateGroupCallbackCommand extends AbstractCallbackCommand {

    /**
     * 操作的群 ID
     */
    @JsonProperty("GroupId")
    private String groupId;

    /**
     * 发起创建群组请求的操作者 Identifier
     */
    @JsonProperty("Operator_Account")
    private String operatorAccount;

    /**
     * 发起创建群组请求的操作者 Identifier
     */
    @JsonProperty("Owner_Account")
    private String ownerAccount;

    /**
     * 产生群消息的 群组形态介绍，例如 Private，Public 和 ChatRoom
     */
    @JsonProperty("Type")
    private String type;

    /**
     * 请求创建的群组的名称
     */
    @JsonProperty("Name")
    private String name;

    /**
     * 该用户已创建的同类的群组个数
     */
    @JsonProperty("CreatedNum")
    private Integer createdNum;

    /**
     * 请求创建的群组的初始化成员列表
     */
    @JsonProperty("MemberList")
    private List<GroupMember> memberList;

    /**
     * 用户建群时的自定义字段
     */
    @JsonProperty("UserDefinedDataList")
    private List<GroupUserDefinedData> userDefinedDataList;
}
