package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

/**
 * 群组满员之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AfterGroupFullCallbackCommand extends AbstractCallbackCommand {

    /**
     * 产生群消息的群组 ID
     */
    @JsonProperty("GroupId")
    private String groupId;
}
