package net.guerlab.smart.qcloud.im.callbackcommand.state;

/**
 * 用户上线或者下线的动作
 *
 * @author guer
 */
public enum StateChangeAction {

    /**
     * 上线（TCP 建立）
     */
    Login,
    /**
     * 下线（TCP 断开）
     */
    Logout,
    /**
     * 网络断开（TCP 断开）
     */
    Disconnect
}
