package net.guerlab.smart.qcloud.im.callbackcommand.sns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

import java.util.List;

/**
 * 添加好友之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FriendAddCallbackCommand extends AbstractCallbackCommand {

    /**
     * 触发回调的命令字：
     * 加好友请求，合理的取值如下：friend_add、FriendAdd
     * 加好友回应，合理的取值如下：friend_response、FriendResponse
     */
    @JsonProperty("ClientCmd")
    private String clientCmd;

    /**
     * 如果当前请求是后台触发的加好友请求，则该字段被赋值为管理员帐号；否则为空
     */
    @JsonProperty("Admin_Account")
    private String adminAccount;

    /**
     * 管理员强制加好友标记：1 表示强制加好友；0 表示常规加好友方式
     */
    @JsonProperty("ForceFlag")
    private Integer forceFlag;

    /**
     * 成功添加的好友对
     */
    @JsonProperty("PairList")
    private List<Pair> pairList;
}
