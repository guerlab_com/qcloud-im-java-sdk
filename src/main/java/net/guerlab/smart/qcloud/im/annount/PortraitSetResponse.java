package net.guerlab.smart.qcloud.im.annount;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.AbstractImResponse;

/**
 * 设置资料响应
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PortraitSetResponse extends AbstractImResponse {

    /**
     * 详细的客户端展示信息
     */
    @JsonProperty("ErrorDisplay")
    private String errorDisplay;
}
