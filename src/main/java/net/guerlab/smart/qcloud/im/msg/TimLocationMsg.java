package net.guerlab.smart.qcloud.im.msg;

/**
 * 地理位置消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class TimLocationMsg extends AbstractMsgBody<TimLocationMsgContent> {

    public TimLocationMsg() {
        super(MsgType.TIMLocationElem);
    }
}
