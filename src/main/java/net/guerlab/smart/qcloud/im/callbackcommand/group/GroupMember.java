package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 群组用户
 *
 * @author guer
 */
@Data
public class GroupMember {

    /**
     * 用户
     */
    @JsonProperty("Member_Account")
    private String memberAccount;
}
