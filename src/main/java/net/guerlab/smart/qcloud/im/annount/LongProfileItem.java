package net.guerlab.smart.qcloud.im.annount;

/**
 * 资料设置项-uint64_t类型
 *
 * @author guer
 */
public class LongProfileItem extends ProfileItem<Long> {

    /**
     * 无参数构造
     */
    public LongProfileItem() {
    }

    /**
     * 通过指定标签和值来构造一个资料设置项
     *
     * @param tag
     *         标签
     * @param value
     *         值
     */
    public LongProfileItem(String tag, Long value) {
        super(tag, value);
    }
}
