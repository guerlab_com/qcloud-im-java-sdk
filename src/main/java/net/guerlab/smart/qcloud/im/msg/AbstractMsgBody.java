package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

/**
 * 抽象消息内容
 *
 * @author guer
 */
@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "MsgType")
@JsonSubTypes({ @JsonSubTypes.Type(name = "TIMTextElem", value = TimTextMsg.class),
        @JsonSubTypes.Type(name = "TIMFaceElem", value = TimFaceMsg.class),
        @JsonSubTypes.Type(name = "TIMLocationElem", value = TimLocationMsg.class),
        @JsonSubTypes.Type(name = "TIMCustomElem", value = TimCustomMsg.class),
        @JsonSubTypes.Type(name = "TIMSoundElem", value = TimSoundMsg.class),
        @JsonSubTypes.Type(name = "TIMImageElem", value = TimImageMsg.class),
        @JsonSubTypes.Type(name = "TIMFileElem", value = TimFileMsg.class),
        @JsonSubTypes.Type(name = "TIMVideoFileElem", value = TimVideoFileMsg.class) })
public class AbstractMsgBody<C extends AbstractMsgContent> {

    /**
     * 消息对象类型
     */
    @JsonProperty("MsgType")
    private MsgType msgType;

    /**
     * 消息内容
     */
    @JsonProperty("MsgContent")
    private C msgContent;

    /**
     * 构造消息内容对象
     */
    public AbstractMsgBody() {
    }

    /**
     * 通过消息对象类型构造消息内容对象
     *
     * @param msgType
     *         消息对象类型
     */
    public AbstractMsgBody(MsgType msgType) {
        this.msgType = msgType;
    }
}
