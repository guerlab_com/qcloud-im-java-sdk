package net.guerlab.smart.qcloud.im.annount;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.AbstractImRequest;
import net.guerlab.smart.qcloud.im.ImException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 设置资料请求
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PortraitSetRequest extends AbstractImRequest<PortraitSetResponse> {

    /**
     * 需要设置该 Identifier 的资料
     */
    private String fromAccount;

    /**
     * 资料设置项列表
     */
    private List<ProfileItem> profileItems = new ArrayList<>();

    /**
     * 添加资料设置项
     *
     * @param profileItem
     *         资料设置项
     */
    public void addProfileItem(ProfileItem profileItem) {
        profileItems.add(profileItem);
    }

    @Override
    public String uri() {
        return "v4/profile/portrait_set";
    }

    @Override
    public byte[] requestBody(ObjectMapper objectMapper) {
        try {
            HashMap<String, Object> jsonObject = new HashMap<>(2);

            jsonObject.put("From_Account", fromAccount);
            jsonObject.put("ProfileItem", profileItems);

            return objectMapper.writeValueAsBytes(jsonObject);
        } catch (Exception e) {
            throw new ImException(e.getLocalizedMessage(), e);
        }
    }
}
