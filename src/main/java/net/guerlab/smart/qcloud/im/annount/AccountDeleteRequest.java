package net.guerlab.smart.qcloud.im.annount;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.AbstractImRequest;
import net.guerlab.smart.qcloud.im.ImException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 删除用户请求
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AccountDeleteRequest extends AbstractImRequest<AccountDeleteResponse> {

    /**
     * 请求删除的帐号对象数组，单次请求最多支持100个帐号
     */
    private List<DeleteItem> deleteItems = new ArrayList<>();

    /**
     * 添加请求删除的帐号对象
     *
     * @param deleteItem
     *         请求删除的帐号对象
     */
    public void addDeleteItem(DeleteItem deleteItem) {
        deleteItems.add(deleteItem);
    }

    @Override
    public String uri() {
        return "v4/im_open_login_svc/account_delete";
    }

    @Override
    public byte[] requestBody(ObjectMapper objectMapper) {
        try {
            HashMap<String, Object> jsonObject = new HashMap<>(1);

            jsonObject.put("DeleteItem", deleteItems);

            return objectMapper.writeValueAsBytes(jsonObject);
        } catch (Exception e) {
            throw new ImException(e.getLocalizedMessage(), e);
        }
    }

    @Data
    public static class DeleteItem {

        /**
         * 请求删除的帐号的 UserID
         */
        @JsonProperty("UserID")
        private String userId;
    }
}
