package net.guerlab.smart.qcloud.im.msg;

/**
 * 文件消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
public class TimFileMsg extends AbstractMsgBody<TimFileMsgContent> {

    public TimFileMsg() {
        super(MsgType.TIMFileElem);
    }
}
