package net.guerlab.smart.qcloud.im.msg;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import net.guerlab.smart.qcloud.im.AbstractImRequest;
import net.guerlab.smart.qcloud.im.ImException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 发送消息请求
 *
 * @author guer
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class SendMsgRequest extends AbstractImRequest<SendMsgResponse> {

    /**
     * 是否消息同步至 From_Account
     */
    private boolean syncOtherMachine;

    /**
     * 消息发送方 Identifier（用于指定发送消息方帐号）
     */
    private String fromAccount;

    /**
     * 消息接收方 Identifier
     */
    private String toAccount;

    /**
     * 消息离线保存时长（单位：秒），最长为7天（604800秒）。若消息只发在线用户，不想保存离线，则该字段填0。若不填，则默认保存7天
     */
    private Integer msgLifeTime;

    /**
     * 消息随机数，由随机函数产生（标记该条消息，用于后台定位问题）
     */
    private Integer msgRandom;

    /**
     * 消息时间戳，UNIX 时间戳（单位：秒）
     */
    private Integer msgTimeStamp;

    /**
     * 消息内容
     */
    private List<AbstractMsgBody> msgBodyList = new ArrayList<>();

    /**
     * 添加消息内容
     *
     * @param msgBody
     *         消息内容
     */
    public void addMsgBody(AbstractMsgBody msgBody) {
        msgBodyList.add(msgBody);
    }

    @Override
    public String uri() {
        return "v4/openim/sendmsg";
    }

    @Override
    public byte[] requestBody(ObjectMapper objectMapper) {
        try {
            HashMap<String, Object> jsonObject = new HashMap<>(8);

            jsonObject.put("SyncOtherMachine", syncOtherMachine ? 1 : 2);
            if (fromAccount != null) {
                jsonObject.put("From_Account", fromAccount);
            }
            jsonObject.put("To_Account", toAccount);
            if (msgLifeTime != null) {
                jsonObject.put("MsgLifeTime", msgLifeTime);
            }
            jsonObject.put("MsgRandom", msgRandom);
            if (msgTimeStamp != null) {
                jsonObject.put("MsgTimeStamp", msgTimeStamp);
            }
            jsonObject.put("MsgBody", msgBodyList);

            log.debug("json : {}", objectMapper.writeValueAsString(jsonObject));

            return objectMapper.writeValueAsBytes(jsonObject);
        } catch (Exception e) {
            throw new ImException(e.getLocalizedMessage(), e);
        }
    }
}
