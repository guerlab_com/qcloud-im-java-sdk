package net.guerlab.smart.qcloud.im.msg;

/**
 * 消息对象类型
 *
 * @author guer
 */
public enum MsgType {
    /**
     * 文本消息
     */
    TIMTextElem,

    /**
     * 表情消息
     */
    TIMFaceElem,

    /**
     * 位置消息
     */
    TIMLocationElem,

    /**
     * 自定义消息
     */
    TIMCustomElem,

    /**
     * 语音消息元素
     */
    TIMSoundElem,

    /**
     * 图像消息元素
     */
    TIMImageElem,

    /**
     * 文件消息元素
     */
    TIMFileElem,

    /**
     * 视频消息元素
     */
    TIMVideoFileElem,
}
