package net.guerlab.smart.qcloud.im;

/**
 * IM常量
 *
 * @author guer
 */
public interface ImConstants {

    /**
     * 默认编码集名称
     */
    String DEFAULT_CHARSET_NAME = "UTF-8";
    /**
     * 默认数据类型
     */
    String DEFAULT_MEDIA_TYPE = "application/json";

    /**
     * 默认签名方式
     */
    String DEFAULT_SIGN_TYPE = "HmacSHA256";

    /**
     * 默认过期时间
     */
    long DEFAULT_EXPIRE = 86400;

    /**
     * 基础路径
     */
    String BASE_URL = "https://console.tim.qq.com/";

    /**
     * 成功错误码
     */
    int CODE_SUCCESS = 0;

    String PARAMS_SDK_APP_ID = "sdkappid";
    String PARAMS_IDENTIFIER = "identifier";
    String PARAMS_USERSIG = "usersig";
    String PARAMS_RANDOM = "random";
    String PARAMS_CONTENT_TYPE = "contenttype";
    String CONTENT_TYPE_JSON = "json";

    /*
     * 回调命令 - start
     */
    /**
     * 回调命令 - 状态变更回调
     */
    String CALLBACK_COMMAND_STATE_STATECHANGE = "State.StateChange";
    /**
     * 回调命令 - 添加好友之后回调
     */
    String CALLBACK_COMMAND_SNS_FRIEND_ADD = "Sns.CallbackFriendAdd";
    /**
     * 回调命令 - 添加好友之后回调
     */
    String CALLBACK_COMMAND_SNS_FRIEND_DELETE = "Sns.CallbackFriendDelete";
    /**
     * 回调命令 - 添加黑名单之后回调
     */
    String CALLBACK_COMMAND_SNS_BLACK_LIST_ADD = "Sns.CallbackBlackListAdd";
    /**
     * 回调命令 - 删除黑名单之后回调
     */
    String CALLBACK_COMMAND_SNS_BLACK_LIST_DELETE = "Sns.CallbackBlackListDelete";
    /**
     * 回调命令 - 发单聊消息之前回调
     */
    String CALLBACK_COMMAND_C2C_BEFORE_SEND_MSG = "C2C.CallbackBeforeSendMsg";
    /**
     * 回调命令 - 发单聊消息之后回调
     */
    String CALLBACK_COMMAND_C2C_AFTER_SEND_MSG = "C2C.CallbackAfterSendMsg";
    /**
     * 回调命令 - 创建群组之前回调
     */
    String CALLBACK_COMMAND_GROUP_BEFORE_CREATE_GROUP = "Group.CallbackBeforeCreateGroup";
    /**
     * 回调命令 - 创建群组之后回调
     */
    String CALLBACK_COMMAND_GROUP_AFTER_CREATE_GROUP = "Group.CallbackAfterCreateGroup";
    /**
     * 回调命令 - 申请入群之前回调
     */
    String CALLBACK_COMMAND_GROUP_BEFORE_APPLY_JOIN_GROUP = "Group.CallbackBeforeApplyJoinGroup";
    /**
     * 回调命令 - 拉人入群之前回调
     */
    String CALLBACK_COMMAND_GROUP_BEFORE_INVITE_JOIN_GROUP = "Group.CallbackBeforeInviteJoinGroup";
    /**
     * 回调命令 - 新成员入群之后回调
     */
    String CALLBACK_COMMAND_GROUP_AFTER_NEW_MEMBER_JOIN = "Group.CallbackAfterNewMemberJoin";
    /**
     * 回调命令 - 群成员离开之后回调
     */
    String CALLBACK_COMMAND_GROUP_AFTER_MEMBER_EXIT = "Group.CallbackAfterMemberExit";
    /**
     * 回调命令 - 群内发言之前回调
     */
    String CALLBACK_COMMAND_GROUP_BEFORE_SEND_MSG = "Group.CallbackBeforeSendMsg";
    /**
     * 回调命令 - 群内发言之前回调
     */
    String CALLBACK_COMMAND_GROUP_AFTER_SEND_MSG = "Group.CallbackAfterSendMsg";
    /**
     * 回调命令 - 群组满员之后回调
     */
    String CALLBACK_COMMAND_GROUP_AFTER_GROUP_FULL = "Group.CallbackAfterGroupFull";
    /**
     * 回调命令 - 群组解散之后回调
     */
    String CALLBACK_COMMAND_GROUP_AFTER_GROUP_DESTROYED = "Group.CallbackAfterGroupDestroyed";
    /**
     * 回调命令 - 群组资料修改之后回调
     */
    String CALLBACK_COMMAND_GROUP_AFTER_GROUP_INFO_CHANGED = "Group.CallbackAfterGroupInfoChanged";
    /*
     * 回调命令 - end
     */
}
