package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

/**
 * 申请入群之前回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BeforeApplyJoinGroupCallbackCommand extends AbstractCallbackCommand {

    /**
     * 产生群消息的群组 ID
     */
    @JsonProperty("GroupId")
    private String groupId;

    /**
     * 产生群消息的 群组形态介绍，例如 Private，Public 和 ChatRoom
     */
    @JsonProperty("Type")
    private String type;

    /**
     * 申请加群者 Identifier
     */
    @JsonProperty("Requestor_Account")
    private String requestorAccount;
}
