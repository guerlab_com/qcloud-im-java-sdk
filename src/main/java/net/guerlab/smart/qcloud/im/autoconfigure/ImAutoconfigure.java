package net.guerlab.smart.qcloud.im.autoconfigure;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.guerlab.smart.qcloud.im.ImClient;
import net.guerlab.smart.qcloud.im.client.OkHttpImClient;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

/**
 * Im自动配置
 *
 * @author guer
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(ImProperties.class)
public class ImAutoconfigure {

    private static final int DEFAULT_TIMEOUT = 10;

    private OkHttpClient okHttpClient;

    /**
     * 创建http请求客户端
     *
     * @return http请求客户端
     */
    private static OkHttpClient createHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        builder.readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        builder.writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        SSLSocketFactory sslSocketFactory = createSslSocketFactory();
        if (sslSocketFactory != null) {
            builder.sslSocketFactory(sslSocketFactory, TrustAllManager.INSTANCE);
        }
        builder.hostnameVerifier((hostname, session) -> true);

        return builder.build();
    }

    private static SSLSocketFactory createSslSocketFactory() {
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[] { TrustAllManager.INSTANCE }, new SecureRandom());
            return sc.getSocketFactory();
        } catch (Exception e) {
            log.debug(e.getLocalizedMessage(), e);
            return null;
        }
    }

    @Autowired(required = false)
    public void setOkHttpClient(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    /**
     * 设置控制器映射
     *
     * @param properties
     *         Im配置
     * @return ImClient
     */
    @Bean
    public ImClient imClient(ImProperties properties) {
        Long sdkAppId = properties.getSdkAppId();
        String key = properties.getKey();
        String identifier = properties.getIdentifier();

        Assert.notNull(sdkAppId, "sdkAppId cannot be null");
        Assert.notNull(key, "key cannot be null");
        Assert.notNull(identifier, "identifier cannot be null");

        return new OkHttpImClient(sdkAppId, key, identifier, client(), new ObjectMapper());
    }

    private OkHttpClient client() {
        if (okHttpClient != null) {
            return okHttpClient;
        }

        return createHttpClient();
    }

    private static class TrustAllManager implements X509TrustManager {

        static final TrustAllManager INSTANCE = new TrustAllManager();

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) {
            /*
             * ignore
             */
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) {
            /*
             * ignore
             */
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[] {};
        }
    }
}
