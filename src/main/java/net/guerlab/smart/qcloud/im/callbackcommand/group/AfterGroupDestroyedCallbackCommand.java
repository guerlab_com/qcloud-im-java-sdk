package net.guerlab.smart.qcloud.im.callbackcommand.group;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.guerlab.smart.qcloud.im.callbackcommand.AbstractCallbackCommand;

import java.util.List;

/**
 * 群组解散之后回调
 *
 * @author guer
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AfterGroupDestroyedCallbackCommand extends AbstractCallbackCommand {

    /**
     * 被解散的群组 ID
     */
    @JsonProperty("GroupId")
    private String groupId;

    /**
     * 被解散群的 群组形态介绍，例如 Private，Public 和 ChatRoom
     */
    @JsonProperty("Type")
    private String type;

    /**
     * 发起创建群组请求的操作者 Identifier
     */
    @JsonProperty("Owner_Account")
    private String ownerAccount;

    /**
     * 被解散群的成员列表
     */
    @JsonProperty("MemberList")
    private List<GroupMember> memberList;
}
