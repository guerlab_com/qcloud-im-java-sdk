package net.guerlab.smart.qcloud.im.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频消息元素
 *
 * @author guer
 */
@SuppressWarnings("WeakerAccess")
@Data
@EqualsAndHashCode(callSuper = true)
public class TimVideoFileMsg extends AbstractMsgBody<TimVideoFileMsgContent> {

    public TimVideoFileMsg() {
        super(MsgType.TIMVideoFileElem);
    }
}
